Hello! C.Rogers, here. If you're reading this, you're the unfortunate soul who has inherited these files... possibly to build the next video, or maybe you just want to see how it was done for your own FLOSS animation purposes.

While I've done my best to clean up and simplify the original file structure, no doubt I probably missed something, somewhere. If you happen to find an error, do help the next person by submitting a patch to fix what's wrong.

The first thing you'll need is Blender 2.81 or greater. This file relies on the new EEVEE rendering engine, which takes a small fraction of the render time of Cycles, not to mention previews in near real-time on somewhat less than powerful hardware.

I've used the "import images as planes" extension to pull in the flat graphics I've exported to the "graphics" folder, and you'll find the svg source file with all the bits in the same folder as this readme (gnome_3.34_release_video_graphics_construct.svg).

This time I've converted all frame output to video files, and replaced them with rendered videos (all added to the "clips" folder), so should be somewhat easier to manage than the last video. :D

All other character animations are rendered directly into /clips from the blender file:  gnome_3.36_2d_animation.blend

Good luck!
