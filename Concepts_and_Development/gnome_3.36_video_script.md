# GNOME 3.36 Release Video
## Creative Brief
This video should play off the last video, with similar flat shaded style and elements. The goal is to show the main release features interspersed with animations which add humour and light-hearted fun to the release announcement.

## Draft outline: Revision 3 (Final draft for Voiceover)

### Intro:
**1. "The GNOME team is back with a fresh new 3.36 release. This version brings a substantial number of new features and design updates."**

[Intro animation shows a few gnome developers typing in various locations (maybe a 4-way screen split, with 4 different locations). Globe expands in the middle of the 4 way split with word bubbles, representing discussion from all over the world]

### Feature 1: Visual style improvements
**2. "With every release, our goal is to provide a more polished, better looking and easier to use experience."**

**3. "3.36 represents a major user experience overhaul, with a multitude of changes to the visual style of GNOME.**

**4. "Many of these changes focus on organisation and functional design aesthetics."**

[examples fade from old style to new style of combinations of the following, giving the overall effect of the changes]
[x] the calendar
[x] search
[x] Redesigned system dialogues
[?] the About panel
[X] the Users panel
[X] and some new icons for a more unified look and feel - https://gitlab.gnome.org/Teams/Design/icon-revolution/issues/3

[2 hours]
### Feature 2: small design tweaks
**5. "This includes a myriad of small design changes, which make GNOME easier than ever to use!"**

[Screen casts of each of these]
[X] popovers (info bar with suspend options)
[X] Password peeking
[X] Password box wiggle
[animation of password box wiggle and user using the password peeking icon to correct and log in]

[1 hours]
### Feature 3: Clocks
**6. "Even the clocks and contact apps have been redesigned to add a little more polish and pleasure to your day-to-day use of GNOME."**

[X][Show clocks and contacts apps fading from old versions to new versions]

[2 hours]
### Feature 4: Do not disturb
**7. "Productivity is at the core of GNOME's design. With so many distractions sometimes you just need to switch off."**

[shows user at desk with notifications popping up everywhere, user puts hands on ears as if attempting to block them out. Maybe the cat hops up and flops down of user's keyboard]

[2 hours]
**8. "GNOME 3.36 lets you do this easily with the addition of a Do Not Disturb switch, allowing you to get more work done, while still getting important system notifications."**

[shows animation of notification switch (and resulting icon by the time), and zooms out to the same user, leaning back in chair, with a cup of coffee, working casually undisturbed]

[1 hours]
### Feature 5: App folders
**9. "GNOME's app folders now appear as dialogues in the centre of the screen, and allow the folder name to be changed inside the dialogue."**
[screencast of new app folder behaviour, dragging icon over folder and changing folder name inside the dialogue]

[1 hours]
### Feature 6: General
**10. "Settings are now more simple to navigate with better organisation, so it's easy to find what you're looking for."**
[screencast of navigation of settings]

[1 hours]
### Feature 7: Privacy
**11. "Privacy controls have been improved, allowing you to view the permissions an app requires."**
[show screencast of allowing and disallowing access to various services, and animated icons, for webcam, microphone, network, etc. along the side]

[1 hours]
### Feature 8: Metered data support
**12. "GNOME can now pause automatic updates when you're on a metered network to save data. You can also manually choose which networks are metered in network settings."**

[show screencast of GNOME detecting metered mobile network, then of user setting a network to metered (or not metered)]

[1 hours]
### Feature 9: Extensions App
**13. "GNOME Shell Extensions are now easier than ever to manage using the new extensions app! You can now see what's installed, what's enabled, and change preferences in one convenient location."**

[screencast showing: enabling/disabling extensions, changing extension preferences, updating and removing extensions]

[1 hours]
### Feature 10: Login/unlock UI
**14. "The lock and login screens have been merged into one, so your notifications now show on the lock screen along with the login dialogue, and require only a click instead of a drag-up action."**
[screencast (or animation) of one-click access to login]

**15. "The user avatar and password entry have both been given a nice new layout, and GNOME now automatically sets your login screen as a blurred version of your wallpaper for a consistent, sleek and attractive style."**

**16. "With all these changes, logging in and unlocking should feel quicker and smoother than ever before."**

[screencast showing how nice and smooth the process is]

[2 hours]
**17. "Well... if you can remember your password, that is!"**

[show user trying to enter password over and over and the password box wiggling]

[3 hours]
### Outro:
**18. "With each release, we're excited to bring you a truckload of awesome features and improvements."**

[shows a user at a desk on a laptop, and a truck is backing up behind them with AWESOME written on the side. The truck back opens, spilling out a huge pile of app icons, and the cat on top. The user then pops up out of the app icons, and gives a thumbs up to the driver]

[3 hours]
**19. "We hope you enjoy using GNOME, and if you're new, come join our great community!"**

[zooms out from truck to gnome developers standing around the truck waving at the camera]
[camera pans upwards into the sky where the GNOME logo is (white on blue as before)]
[end info screens as previous video]
[music fades out]
[end]
